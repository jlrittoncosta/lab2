+++
title = 'Muscle-up'
date = 2023-09-25T21:08:25-03:00
draft = false
+++

## Descrição do exercício:

**Passo 1: Prepare-se**

-Encontre uma barra fixa ou uma barra de ginástica suspensa em uma altura adequada.

-Inicialmente, pode ser útil praticar com uma barra mais baixa ou usar um par de argolas de ginástica, pois oferecem mais estabilidade.

**Passo 2: Pegada e Posição Inicial**

-Posicione-se sob a barra com uma pegada pronada (palmas voltadas para fora) e as mãos afastadas um pouco mais do que a largura dos ombros.

-Pendure-se na barra com os braços completamente estendidos e os pés ligeiramente afastados do chão.

**Passo 3: A Puxada**

-Inicie o movimento puxando o corpo para cima, como faria em um pull-up convencional.

-Concentre-se em usar os músculos das costas e dos braços para levantar o corpo em direção à barra.

-Continue puxando até que seu peito esteja na altura da barra ou um pouco acima dela.

**Passo 4: A Transição**

-Aqui está a parte crítica do muscle-up. Quando seu peito estiver na altura da barra, comece a inclinar o corpo para frente e sobre a barra.

-Ao mesmo tempo, flexione os cotovelos e leve as mãos por cima da barra enquanto mantém os pulsos virados para fora.

-Tente manter o corpo próximo à barra à medida que passa para o lado oposto dela. Esta é a transição que separa o muscle-up de um pull-up comum.

**Passo 5: O Fundo da Barra**

-Após passar para o lado oposto da barra, estenda completamente os braços na posição de fundo da barra.

-Mantenha o tronco ereto e os pés juntos.

**Passo 6: A Descida**

-Para descer, dobre os cotovelos e comece a inverter a transição, movendo o corpo para trás sobre a barra.

-Quando o peito estiver novamente na altura da barra, comece a estender os braços e volte à posição inicial de puxada.

## Progressão:

Devido a ser um exércicio com nível de dificuldade mais elevado é recomendado primeiro o desenvolvimento das áreas do corpo que são exigidas em sua execução através dos seguintes exercícios de progressão:

-Barra

-Dips na barra paralela

-Flexões de Tríceps na Barra Fixa

-Negativos de Muscle-Up