+++
title = 'Bananeira'
date = 2023-09-25T21:09:10-03:00
draft = false
+++

## Descrição do Exercício:

**Passo 1: Preparação**

-Encontre um local espaçoso e seguro para praticar, de preferência com um piso macio ou colchão para amortecer quedas.

**Passo 2: Posicionamento das Mãos**

-Comece de pé, de frente para o local onde deseja executar a bananeira.

-Agache-se e coloque as mãos no chão, aproximadamente a uma distância dos ombros, com os dedos apontados para a frente.

-Espalhe os dedos das mãos para criar uma base sólida.

**Passo 3: Elevação do Corpo**

-Com as mãos firmemente apoiadas no chão, incline o peso do corpo para frente enquanto levanta uma perna para o alto.

-Impulsione a outra perna para cima, estendendo-a na mesma direção da primeira.

-À medida que as pernas se elevam, você se encontrará em uma posição de "cavalo" com o corpo invertido, mas ainda com os pés no chão.

**Passo 4: Equilíbrio**

-Uma vez na posição de "cavalo", concentre-se em manter o equilíbrio, empurrando o chão com as mãos para evitar cair para trás.

-Mantenha os músculos do núcleo e das pernas contraídos para ajudar a manter a posição.

**Passo 5: Alinhamento**

-Tente alinhar o corpo, mantendo a cabeça entre os ombros e os quadris, formando uma linha reta.

-Olhe para um ponto fixo no chão para ajudar no equilíbrio.

**Passo 6: Ajustes**

-Use os dedos das mãos para fazer pequenos ajustes no equilíbrio, movendo as mãos conforme necessário.

-Lembre-se de que é normal balançar um pouco enquanto aperfeiçoa a posição.

**Passo 7: Descida**

-Quando estiver pronto para sair da posição, comece a abaixar uma perna de cada vez, mantendo o controle do movimento.

-Evite cair abruptamente; desça controladamente.

## Progressão:

Devido a ser um exércicio com nível de dificuldade mais elevado é recomendado primeiro o desenvolvimento das áreas do corpo que são exigidas em sua execução através dos seguintes exercícios de progressão:

-Bananeira na Parede

-Bananeira de Tripé

-Bananeira com Antebraço

-Bananeira com Pernas Dobradas