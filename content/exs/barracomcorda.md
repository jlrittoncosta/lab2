+++
title = 'Assistência com Banda Elástica'
date = 2023-09-20T20:55:39-03:00
draft = false
+++

## Descrição do exercício

**Passo 1:Escolha a Banda Elástica Adequada**

   -Escolha uma banda elástica com resistência adequada ao seu nível de força. Bandas elásticas vêm em diferentes cores, cada uma representando uma resistência diferente. Comece com uma banda mais fina e com menor resistência se você for iniciante.

**Passo 2:Prenda a Banda Elástica**

   -Amarre a banda elástica em torno da barra fixa, garantindo que esteja bem fixa.
   
   -Passe a outra extremidade da banda por baixo dos seus pés ou joelhos, dependendo do nível de assistência necessário. Quanto mais perto dos pés, maior a assistência.

**Passo 3:Posição Inicial**

   -Fique em pé sob a barra com os pés ligeiramente afastados.
   
   -Posicione as mãos na barra com uma pegada pronada (palmas voltadas para longe de você), em uma largura um pouco mais ampla que a largura dos ombros.

**Passo 4:Inicie o Movimento**

   -Puxe seu corpo em direção à barra, concentrando-se em usar a força dos braços e das costas.
   
   -A banda elástica fornecerá assistência, tornando mais fácil a parte de cima do movimento.

**Passo 5:Suba até o Queixo Acima da Barra**

   -Continue subindo até que seu queixo esteja acima da barra. Isso completa uma repetição assistida com banda elástica.

**Passo 6:Descida Controlada**

   -Desça com controle, estendendo completamente os braços na posição inicial.
   
   -Mantenha a técnica adequada durante toda a descida.





