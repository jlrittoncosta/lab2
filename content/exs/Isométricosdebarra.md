+++
title = 'Isométricos de Barra Fixa'
date = 2023-09-20T20:55:11-03:00
draft = false
+++

## Descrição do exercício:

**Passo 1:Encontre a Barra Fixa Adequada**

    -Encontre uma barra fixa de pull-up que seja segura e estável. Ela deve estar fixada de forma sólida, seja em um suporte de parede ou uma estrutura de ginásio.

**Passo 2:Posição Inicial**

    -Fique em pé sob a barra fixa com os pés ligeiramente afastados. Certifique-se de que a barra esteja ao alcance dos seus braços estendidos.
    
    -Posicione as mãos na barra com as palmas voltadas para longe de você, em uma pegada pronada (palmas para fora). A largura das mãos pode variar dependendo da preferência pessoal, mas comece com uma largura um pouco mais do que a largura dos ombros.

**Passo 3:Suba na Barra Fixa**

    -Pule ou use um apoio para elevar seu queixo acima da barra fixa, alcançando a posição de queixo sobre a barra.
    
    -Certifique-se de que seu queixo esteja ligeiramente acima da barra, e não apenas tocando-a.

**Passo 4:Mantenha a Posição**

    -Agora, concentre-se em manter essa posição o máximo que puder. Você estará em um estado isométrico, o que significa que os músculos estão trabalhando, mas você não está se movendo.
    
    -Mantenha os músculos das costas, braços e ombros contraídos. Isso é essencial para fortalecer essas áreas.

**Passo 5:Respiração e Tempo**

    -Respire regularmente enquanto mantém a posição. Evite prender a respiração.
    
    -Tente manter a posição por 20-30 segundos no início. À medida que ganha força, você pode aumentar gradualmente o tempo.

**Passo 6:Descida**

    -Para sair do isométrico, desça com controle até que seus braços estejam completamente estendidos e seus pés toquem o chão.