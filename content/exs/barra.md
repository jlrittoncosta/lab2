+++
title = 'Barra'
date = 2023-09-20T10:38:47-03:00
draft = false
+++

## Execução do exércicio:

**Passo 1: Posição Inicial**

-Encontre uma barra fixa ou uma barra de chin-up em uma altura adequada. Certifique-se de que a barra esteja firme e segura.

-Fique de pé embaixo da barra, com os pés ligeiramente afastados, de modo que não toquem o chão durante o exercício.

-Posicione as mãos na barra um pouco mais afastadas que a largura dos ombros, com as palmas voltadas para longe de você. Este é o grip pronado.

**Passo 2: Suspensão**

-Mantenha os braços completamente estendidos.

-Incline-se um pouco para trás, mantendo o tronco ereto, e olhe em frente.

-Mantenha os ombros relaxados e para baixo, não encolhidos.

**Passo 3: Subida**

-Inicie o movimento puxando seu corpo em direção à barra.

-Concentre-se em contrair os músculos das costas e dos braços.

-Mantenha os cotovelos apontados para baixo e próximos ao corpo durante toda a subida.

-Continue subindo até que o queixo esteja acima da barra. Isso completa uma repetição.

**Passo 4: Descida**

-Desça controladamente, estendendo completamente os braços na posição inicial.

-Evite balançar o corpo ou usar impulso para subir; concentre-se na força dos braços e das costas.

-Mantenha o tronco firme durante toda a descida.

## Progressão para a barra:

Devido a ser um exércicio com nível de dificuldade mais elevado é recomendado primeiro o desenvolvimento das áreas do corpo que são exigidas em sua execução através dos seguintes exercícios de progressão:

-Isométricos de Barra Fixa

-Negativos (Descida Lenta)

-Assistência com Banda Elástica