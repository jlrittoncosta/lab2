+++
title = 'Negativos'
date = 2023-09-20T20:55:45-03:00
draft = false
+++

## Descrição do exercício:

**Passo 1:Encontre a Barra Fixa Adequada**

    -Encontre uma barra fixa de pull-up que esteja firmemente presa e segura. Certifique-se de que você possa alcançá-la com os braços estendidos.

**Passo 2:Posição Inicial**

    -Fique de pé sob a barra com os pés ligeiramente afastados, de modo que você não toque o chão durante o exercício.
    
    -Posicione as mãos na barra com uma pegada pronada (palmas para fora) um pouco mais ampla do que a largura dos ombros. Esta é a mesma posição inicial usada para pull-ups.

**Passo 3:Suba na Barra**

    -Pule ou use um suporte para elevar seu queixo acima da barra. Certifique-se de começar na posição de queixo sobre a barra, com os braços flexionados.

**Passo 4:Descida Controlada**

    -Agora, o foco é na parte de descida do movimento. Comece a descer lentamente, estendendo completamente os braços.
    
    -Mantenha o controle do movimento e evite cair rapidamente.
    
    -A descida deve durar pelo menos 3 a 5 segundos, se possível. Quanto mais lenta a descida, mais eficaz será o exercício.

**Passo 5:Respiração e Técnica**

    -Respire regularmente durante a descida, evitando prender a respiração.
    
    -Mantenha o corpo reto e evite balançar ou arquear as costas.

**Passo 6:Pousar no Chão**

    -Quando seus braços estiverem completamente estendidos e você estiver de volta à posição inicial, pouse suavemente no chão.